# Layouts 

Layouts module is a library of layout plugins intended for Drupal 8.5.x and later.
Now this module is on the experimrntal;

## Dependency

Layout has a dependency on an image style named "background";
Please create that style before installing this module.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Features and using
Classes can be added to any of the custom layouts provided by this module. 
Add simple classes to your theme and apply them through the layout_builder user interface.

## Maintainers 

- Kostia Bohach ([_shy](https://www.drupal.org/u/_shy))
- Kris Vanderwater  ([eclipsegc](https://www.drupal.org/u/eclipsegc))
- Christopher Skene  ([xtfer](https://www.drupal.org/u/xtfer))


